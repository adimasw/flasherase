`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    06:36:48 04/27/2018 
// Design Name: 
// Module Name:    flasherase_top 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////


module flasherase_top(
    input system_clk_p,
	 input system_clk_n,
    output bpi_we_n,
    output bpi_ce_n,
    output bpi_oe_n,
    output [25:0] bpi_addr_cmd,
	 output bpi_adv,
	 output led_0,
	 output led_1,
	 output led_2,
	 output led_3,
    inout [15:0] bpi_data
    );
	 
clockgen clkgen(.CLK_IN1_P(system_clk_p), .CLK_IN1_N(system_clk_n),.CLK_OUT1(clk));	 

wire[25:0] offset=26'h010000;
reg[15:0] data;
reg we_n=1;
reg ce_n=0;
reg oe_n=1;
reg adv=0;
reg led=0;
reg[25:0] addr=26'h000000;
reg[3:0] status;
reg[3:0] state=0;

assign led_0=status[0];
assign led_1=status[1];
assign led_2=status[2];
assign led_3=status[3];

assign bpi_addr_cmd=addr;
assign bpi_we_n=we_n;
assign bpi_oe_n=oe_n;
assign bpi_ce_n=ce_n;
assign bpi_adv=adv;
assign bpi_data=data;

always @(posedge clk)
begin
	case (state)
		4'h0:
		begin
			we_n<=0;
			data<=16'h60;
			state<=4'h01;
		end
		4'h01:
		begin
			we_n<=1;
			state<=4'h02;
		end
		4'h02:
		begin
			we_n<=0;
			data<=16'hD0;
			state<=4'h03;
		end
		4'h03:
		begin
			we_n<=1;
			state<=4'h4;
		end
		4'h4:	
		begin
			we_n<=0;
			ce_n<=0;
			data<=16'h20;
			state<=4'h5;
		end
		4'h5:
		begin
			we_n<=1;
			state<=4'h6;
		end
		4'h6:
		begin
			we_n<=0;
			adv<=1;
			data<=16'hD0;
			state<=4'h7;
		end
		4'h7:
		begin
			we_n<=1;
			adv<=0;
			oe_n<=0;
			state<=4'h8;
			data<=16'hz;
		end
		4'h8:
		begin
			state<=4'hc;
		end
		4'h9:
		begin
		oe_n<=1;
		status<=bpi_data[7:4];
			if (bpi_data[7]==1)
				state<=4'ha;
			else
				state<=4'h7;
		end
		4'ha:
		begin
		//if (addr==26'h3FE0000)
		if (addr==26'h010000)
			state<=4'hb;
		else
			begin
				addr<=addr+offset;
				state<=4'h0;
			end
		end
		4'hb:
		begin
			we_n<=1;
			oe_n<=1;
			ce_n<=1;
		end
		4'hc:
			state<=4'h9;
	endcase
end
endmodule
